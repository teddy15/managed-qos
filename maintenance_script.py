#!/usr/bin/python

import sys
import subprocess
from threading import Timer,Thread
import argparse
import os
import time
import os.path
from os import path
from datetime import datetime, timedelta, date
from distutils.version import StrictVersion
import random

sys.path.append("/Users/check/Desktop/maintenance_daemon-master/firebase_admin_module")
import firebase_admin
from firebase_admin import db

version_number = "17november2021"
plist_path = "/Users/check/Desktop/maintenance_daemon-master/com.mic.Reboot.plist"
checkvar = 0
force_reboot_high_cpu = False


kill = lambda process: process.kill()
# ----------------------------------#
gitoutputcheck = ""

def check_backup():
    global tmectl_info
    tmectl_info = subprocess.check_output("/usr/local/bin/tmectl info 2>&1 | grep 'Next run'", shell=True).encode(
        "utf-8").strip()


def check_backup_and_storage():
    if checkvar == 1:
        print "Checking: check_backup_and_storage"
        exit(1)
    subprocess.Popen([
                         'date +"%m %d %Y %I:%M %p: Currently checking for Valid Backups within the last 48 Hours" >> /users/check/desktop/maintenance_log.txt'],
                     shell=True, stdout=subprocess.PIPE)
    backup_within_last_two_days = subprocess.check_output(
        "/usr/bin/log show --style syslog --info --last 2d --predicate \'processImagePath contains \"backupd\" and subsystem beginswith \"com.apple.TimeMachine\"\' | sed \'s/..*o]//\' | grep \"Backup completed successfully\" | tail -1",
        shell=True).decode("utf-8")
    current_storage = subprocess.check_output("df -h /  | awk 'NR==2{print $4}' | sed 's/[^0-9]*//g'",
                                              shell=True).decode("utf-8")
    if backup_within_last_two_days:
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Backup has been completed within 48 hours" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
    else:
        subprocess.Popen(
            ['date +"%m %d %Y %I:%M %p: No Backup found within 48 hours" >> /users/check/desktop/maintenance_log.txt'],
            shell=True, stdout=subprocess.PIPE)
    subprocess.Popen(['date +"%m %d %Y %I:%M %p: Checking Free Storage" >> /users/check/desktop/maintenance_log.txt'],
                     shell=True, stdout=subprocess.PIPE)
    if current_storage < 40:
        subprocess.Popen([
                             'date +\"%m %d %Y %I:%M %p: Warning Available Free Space is: \" | perl -pe \'chomp\' >> /users/check/desktop/maintenance_log.txt && df -h / | awk \'NR==2{print $4}\' >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
    else:
        subprocess.Popen([
                             'date +\"%m %d %Y %I:%M %p: Available Free Space is: \" | perl -pe \'chomp\' >> /users/check/desktop/maintenance_log.txt && df -h / | awk \'NR==2{print $4}\' >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)


def set_reboot():
    reboot_plist_check_minute = subprocess.check_output(
        "sudo /usr/libexec/PlistBuddy -c \"Print :StartCalendarInterval:0:Minute\" " + str(plist_path),
        shell=True).encode("utf-8").replace('\n', '')
    reboot_plist_check_hour = subprocess.check_output(
        "sudo /usr/libexec/PlistBuddy -c \"Print :StartCalendarInterval:0:Hour\" " + str(plist_path),
        shell=True).encode("utf-8").replace('\n', '')
    print ("Set Reboot Time Before Reboot Time Check: ", reboot_plist_check_hour, ":", reboot_plist_check_minute)
    if reboot_plist_check_minute == '0':
        print ("Default Time for 2:00 detected, Applying new randomized time, to view new time please run the script again")
        subprocess.Popen(['min_res=' + str(random.randint(1,59)) + ';export min_res;sudo /usr/libexec/PlistBuddy -c "Set :StartCalendarInterval:0:Minute ${min_res}" ' + str(plist_path)], shell=True, stdout=subprocess.PIPE)
        reboot_plist_check_minute_new = subprocess.check_output(
            "sudo /usr/libexec/PlistBuddy -c \"Print :StartCalendarInterval:0:Minute\" " + str(plist_path),
            shell=True).encode("utf-8").replace('\n', '')
        reboot_plist_check_hour_new = subprocess.check_output(
            "sudo /usr/libexec/PlistBuddy -c \"Print :StartCalendarInterval:0:Hour\" " + str(plist_path),
            shell=True).encode("utf-8").replace('\n', '')
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: New Time Set:"' + reboot_plist_check_hour_new + ':' + reboot_plist_check_minute_new + ' >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        print ("New Reboot Time Successfully applied, to view new time please run the script again")
    else:
        reboot_plist_check_minute = subprocess.check_output(
            "sudo /usr/libexec/PlistBuddy -c \"Print :StartCalendarInterval:0:Minute\" " + str(plist_path),
            shell=True).decode("utf-8")


def reset_reboot_random():
    subprocess.Popen(['min_res=' + str(random.randint(0,
                                                      59)) + ';export min_res;sudo /usr/libexec/PlistBuddy -c "Set :StartCalendarInterval:0:Minute ${min_res}" ' + str(
        plist_path)],
                     shell=True, stdout=subprocess.PIPE)
    reboot_plist_check_minute = subprocess.check_output(
        "sudo /usr/libexec/PlistBuddy -c \"Print :StartCalendarInterval:0:Minute\" " + str(plist_path),
        shell=True).encode(
        "utf-8").replace('\n', '')
    reboot_plist_check_hour = subprocess.check_output(
        "sudo /usr/libexec/PlistBuddy -c \"Print :StartCalendarInterval:0:Hour\" " + str(plist_path),
        shell=True).encode(
        "utf-8").replace('\n', '')
    print("Set Reboot Time is Now: ", reboot_plist_check_hour, ":", reboot_plist_check_minute)


def check_for_updates():
    if checkvar == 1:
        print "Checking: check_for_updates"
        return 0
    #    if path.exists("/Users/check/Desktop/maintenance_script.py.old") == False:
    #    	subprocess.Popen(['date +"%m %d %Y %I:%M %p: Checking for Updates - Making backup of current Script" >> /users/check/desktop/maintenance_log.txt'], shell=True, stdout=subprocess.PIPE)
    # 	subprocess.Popen(['sudo cp /Users/check/Desktop/maintenance_daemon-master/maintenance_script.py  /Users/check/Desktop/maintenance_script.py.old'], shell=True, stdout=subprocess.PIPE)
    # else:
    # 	subprocess.Popen(['date +"%m %d %Y %I:%M %p: Checking for Updates - Backup of current Script already exists - proceeding with update check" >> /users/check/desktop/maintenance_log.txt'], shell=True, stdout=subprocess.PIPE)
    try:
        global gitoutputcheck
        gitoutputcheck = subprocess.check_output(
            "sudo git -C /Users/check/Desktop/maintenance_daemon-master/ pull origin master", shell=True).decode(
            "utf-8")
    except:
        gitreset = subprocess.check_output("git reset --hard", shell=True).decode("utf-8")
    finally:
        gitoutputcheck = subprocess.check_output(
            "sudo git -C /Users/check/Desktop/maintenance_daemon-master/ pull origin master", shell=True).decode(
            "utf-8")
    file = open("/users/check/desktop/maintenance_log.txt", "a")
    file.write("Version Number: " + version_number + "\n")
    file.write("Checking for updates: " + gitoutputcheck)
    file.close()
    if "Already up to date" not in str(gitoutputcheck):
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Update Complete - Next run will run updated Script..." >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        subprocess.Popen(['sudo python /Users/check/Desktop/maintenance_daemon-master/maintenance_script.py -c'],
                         shell=True, stdout=subprocess.PIPE)
        time.sleep(5)
        # sys.exit()


    else:
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: No New updates proceeding with Current Version" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)


def check_uptime():
    if checkvar == 1:
        print "Checking: check_uptime"
        return 0
    last_boot_time = subprocess.check_output("sysctl kern.boottime | awk '{print $5}' | tr -d ','", shell=True).decode(
        "utf-8").strip()
    current_time = subprocess.check_output("date '+%s'", shell=True).decode("utf-8").strip()
    try:
        lbt = int(last_boot_time)
        ct = int(current_time)
    except:
        print "Unable to detect seconds since last boot"
        return 86500
    time_since_last_boot = ct - lbt
    print 'Seconds since last boot:', time_since_last_boot
    return time_since_last_boot


def check_how_many_users():
    if checkvar == 1:
        print "Checking: check_how_many_users"
        return 0
    subprocess.Popen([
                         'date +"%m %d %Y %I:%M %p: Checking number of concurrently logged in users" >> /users/check/desktop/maintenance_log.txt'],
                     shell=True, stdout=subprocess.PIPE)
    current_cpu_percent_1 = subprocess.check_output(
        "top -l 2 -n 0 -F | egrep -o ' \d*\.\d+% idle' | tail -1 | sed 's/%//'", shell=True).decode("utf-8")
    current_cpu_percent_1 = current_cpu_percent_1.replace('idle', '')
    ave_cpu = 100 - float(current_cpu_percent_1)
    concurrently_logged_users = subprocess.check_output("users | wc -w", shell=True).decode("utf-8")
    try:
        concurrently_logged_users = int(concurrently_logged_users)
    except:
        concurrently_logged_users = 1
    check_backup_and_storage()
    if concurrently_logged_users >= 8:
        print "Killing Login Window Process:"
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: More than 8 users detected - killing loginwindow process" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        my_timer = Timer(30, kill,
                         [subprocess.Popen(['sudo pkill -9 loginwindow'], shell=True, stdout=subprocess.PIPE)])
        try:
            my_timer.start()
        finally:
            my_timer.cancel()
    command = ['date +"%m %d %Y %I:%M %p: The current cpu usage is:"', str(ave_cpu),
               '>> /users/check/desktop/maintenance_log.txt']
    subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    # subprocess.Popen(['date +"%m %d %Y %I:%M %p: Maintenance Check Complete - All tests passed - no reboot requested" >> /users/check/desktop/maintenance_log.txt'], shell=True, stdout=subprocess.PIPE)
    print "The current cpu usage is: ", ave_cpu
    print "Check Complete"
    global force_reboot_high_cpu
    if ave_cpu > 90:
        print "The current cpu usage is: ", ave_cpu
        print "Consider rebooting Server..."
        force_reboot_high_cpu = True
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Maintenance Check - CPU above 90 percent - No Action Taken - Exiting Script " >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        # force_reboot()
        sys.exit()
    else:
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Maintenance Check Complete - All tests passed - no reboot requested" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        force_reboot_high_cpu = False
        print "The current cpu usage is: ", ave_cpu
        print "Check Complete"
        time.sleep(2)
        # sys.exit()


# def reset_daemon_and_git():
# 	subprocess.Popen(['date +"%m %d %Y %I:%M %p: Resetting Script and Daemon" >> /users/check/desktop/maintenance_log.txt'], shell=True, stdout=subprocess.PIPE)
# 	installation = subprocess.check_output("sudo bash -c 'git clone https://gitlab.com/teddy15/managed-qos.git /Users/check/Desktop/maintenance_daemon-master/;sudo cp /Users/check/Desktop/maintenance_daemon-master/com.mic.Maintenance.plist /Library/LaunchDaemons/';launchctl load /Library/LaunchDaemons/com.mic.Maintenance.plist;sudo launchctl load /Library/LaunchDaemons/com.mic.Maintenance.plist;sudo launchctl start com.mic.Maintenance.plist;sleep 5;less /Users/check/Desktop maintenance_log.txt;sudo launchctl list | grep MaintenanceService;", shell=True).decode("utf-8")
# 	file = open("/users/check/desktop/maintenance_log.txt", "a")
#     file.write("Installation: " + installation)
#     file.close()
#     sys.exit()


def has_server_been_rebooted_within_one_day():
    if checkvar == 1:
        print "Checking: has_server_been_rebooted_within_one_day"
        return 0
    if check_uptime() > 86400:
        print("False")
        return False
    else:
        print("True")
        return True


def has_server_just_been_rebooted():
    if checkvar == 1:
        print "Checking: has_server_just_been_rebooted"
        return 0
    if check_uptime() > 1000:
        print("False")
        return False
    else:
        print("True")
        return True


def check_reboot_script():
    if checkvar == 1:
        print "Checking: check_reboot_script"
        return 0
    if has_server_been_rebooted_within_one_day() == True:
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Server has been successfully rebooted within the last day: Proceeding with other checks" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        if has_server_just_been_rebooted == True:
            subprocess.Popen([
                                 'date +"%m %d %Y %I:%M %p: Maintenance Script called after Startup/Reboot" >> /users/check/desktop/maintenance_log.txt'],
                             shell=True, stdout=subprocess.PIPE)
        print("Server has been successfully rebooted within the last day: Proceeding with other checks")
        check_how_many_users()
        # check_for_updates()
        sys.exit()
    else:
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Server has not been rebooted within the last 24 hours: Executing force reboot attempt" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        print("Server needs to be rebooted, executing reboot attempt")
        force_reboot()


def check_rsync():
    if checkvar == 1:
        print "Checking: check_rsync"
        return 0
    number_of_grep_processes = subprocess.check_output(
        "ps ax | grep rsync | grep -v grep | grep -v rsync_check.sh | grep -v colorsync | grep -v colorsyncd | wc -l",
        shell=True).decode("utf-8").strip()
    print("number_of_grep_processes: " + "|" + number_of_grep_processes + "|")
    try:
        grep_proc = int(number_of_grep_processes)
        if grep_proc != 0:  # type conversion may fail in some cases. currently unable to reproduce
            subprocess.Popen([
                                 'date +"%m %d %Y %I:%M %p: rsync Process detected exiting Maintenance Script" >> /users/check/desktop/maintenance_log.txt'],
                             shell=True, stdout=subprocess.PIPE)
            sys.exit()
        else:
            subprocess.Popen([
                                 'date +"%m %d %Y %I:%M %p: No rsync Process detected - Proceeding with Maintenance Script" >> /users/check/desktop/maintenance_log.txt'],
                             shell=True, stdout=subprocess.PIPE)
    except Exception as e:
        print("Error detecting rsync processes: " + str(e))


def force_reboot():
    if checkvar == 1:
        print "Checking: force_reboot"
        return 0
    # print "Rebooting"
    # print "Killing Backup process if it is still running: "
    # my_timer = Timer(30, kill, [subprocess.Popen(['sudo pkill backupd'], shell=True, stdout=subprocess.PIPE)])
    # try:
    #     my_timer.start()
    # finally:
    #     my_timer.cancel()
    #
    # print "Killing Login Window Process:"
    # my_timer = Timer(30, kill, [subprocess.Popen(['sudo pkill -9 loginwindow'], shell=True, stdout=subprocess.PIPE).wait()])
    # try:
    #     my_timer.start()
    # finally:
    #     my_timer.cancel()
    #
    # print "Restarting NuoRDS Service:"
    # my_timer = Timer(30, kill, [
    #     subprocess.Popen(['sudo nrdservice stop;sudo nrdservice start;'], shell=True, stdout=subprocess.PIPE).wait()])
    # try:
    #     my_timer.start()
    # finally:
    #     my_timer.cancel()
    #
    # print "Killing Adobe Creative Cloud Daemon:"
    # my_timer = Timer(30, kill, [subprocess.Popen(['sudo pkill -9 AdobeCRDaemon'], shell=True, stdout=subprocess.PIPE).wait()])
    # try:
    #     my_timer.start()
    # finally:
    #     my_timer.cancel()

    commands = [
        'sudo pkill -9 loginwindow',
        'sudo pkill backupd',
        'sudo pkill -9 AdobeCRDaemon',
        'date +"%m %d %Y %I:%M %p: Killing heavy processes before reboot" >> /users/check/desktop/maintenance_log.txt',
    ]
    # run in parallel
    processes = [subprocess.Popen(cmd, shell=True) for cmd in commands]
    # do other things here..
    # wait for completion
    for p in processes: p.wait()
    global force_reboot_high_cpu
    if force_reboot_high_cpu == False:
        print "Attempting Graceful Restart:"
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Force Reboot Sequence completed - attempting reboot" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        my_timer = Timer(30, kill, [subprocess.Popen(['sudo reboot'], shell=True, stdout=subprocess.PIPE).wait()])
        try:
            my_timer.start()
        finally:
            my_timer.cancel()
        print "Graceful Restart failed attempting force reboot"
        subprocess.Popen(['sudo shutdown -r NOW'], shell=True, stdout=subprocess.PIPE)
    else:
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Executing sudo shutdown -r NOW - attempting reboot" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        subprocess.Popen(['sudo shutdown -r NOW >> /users/check/desktop/maintenance_log.txt'], shell=True,
                         stdout=subprocess.PIPE)
        subprocess.Popen(['sudo shutdown -r NOW'], shell=True, stdout=subprocess.PIPE)


def graceful_reboot_attempt():
    if checkvar == 1:
        print "Checking: graceful_reboot_attempt"
        return 0
    killPIDs = subprocess.check_output(
        "ps axww -o pid,command | grep -v bash | grep [A]pplications/ | grep -v /bin/sh | grep -v [C]asper | grep -v [J]amf | grep -v [S]elf\ Service | grep -v grep | awk '{print $1}'",
        shell=True).decode("utf-8").splitlines()
    # print killPIDs
    killPIDs = ["sudo kill -9 " + s for s in killPIDs]
    # killPIDs=["echo 'Doing this' " + s for s in killPIDs]
    # run in parallel
    processes = [subprocess.Popen(cmd, shell=True) for cmd in killPIDs]
    for x in xrange(len(killPIDs)):
        print("Killing Processes: " + killPIDs[x])
    for p in processes: p.wait()

    heavyProcessesCommand = [
        'sudo pkill backupd',
        'sudo pkill -9 loginwindow',
        'sudo nrdservice stop;sudo nrdservice start;',
        'sudo pkill -9 AdobeCRDaemon',
        'date +"%m %d %Y %I:%M %p: Killing heavy processes before reboot" >> /users/check/desktop/maintenance_log.txt',
    ]
    # run in parallel
    heavyProcesses = [subprocess.Popen(cmd, shell=True) for cmd in heavyProcessesCommand]
    # do other things here..
    # wait for completion
    for p in heavyProcesses: p.wait()

    print "Attempting Graceful Restart:"
    subprocess.Popen([
                         'date +"%m %d %Y %I:%M %p: Pre-Reboot Sequence completed - attempting reboot" >> /users/check/desktop/maintenance_log.txt'],
                     shell=True, stdout=subprocess.PIPE)
    my_timer = Timer(30, kill, [subprocess.Popen(['sudo reboot'], shell=True, stdout=subprocess.PIPE).wait()])
    try:
        my_timer.start()
    finally:
        my_timer.cancel()
    print "Graceful Restart failed attempting force reboot"
    force_reboot()


def syntax_verify():
    global checkvar
    s = 1
    graceful_reboot_attempt()
    force_reboot()
    check_rsync()
    check_reboot_script()
    has_server_just_been_rebooted()
    has_server_been_rebooted_within_one_day()
    check_how_many_users()
    check_uptime()
    check_for_updates()
    check_backup_and_storage()
    s = 0
    # def cancel_os_based_reboot():
    # 	string = "restart at 2:00AM every day"
    # 	output = subprocess.check_output("pmset -g sched", shell=True).decode("utf-8")
    # 	if string in output:
    #     	subprocess.Popen(['date +"%m %d %Y %I:%M %p: Cancelling System based Reboot" >> /users/check/desktop/maintenance_log.txt'], shell=True, stdout=subprocess.PIPE)
    # 		subprocess.Popen(['sudo pmset schedule cancelall;sudo pmset repeat wakeorpoweron MTWRFSU 03:00:00;'], shell=True, stdout=subprocess.PIPE)
    # 	else:
    # 		continue

    sys.exit()


def return_version():
    file = open("/users/check/desktop/maintenance_log.txt", "a")
    file.write("Version Number: " + version_number + "\n")
    # file.write("Checking for updates: " + gitoutputcheck)
    file.close()
    if "Already up to date" not in str(gitoutputcheck):
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Update Complete - Next run will run updated Script..." >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        time.sleep(5)
        sys.exit()
    else:
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: No New updates proceeding with Current Version" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        sys.exit()


def report_to_monitor(reboot_reason):
    cred = firebase_admin.credentials.Certificate(
        "/Users/check/Desktop/maintenance_daemon-master/read-write-serene-flare-296520-84a295c317e8.json")
    firebase_admin.initialize_app(cred, {
        'databaseURL': 'https://serene-flare-296520.firebaseio.com/'
    })

    host_name = subprocess.check_output("scutil --get ComputerName", shell=True).strip().replace(" ", "")
    print("Host Name: " + host_name)
    if "." in host_name:
        print("Error: host name contains invalid character .")
        print("Exiting...")
        exit(1)

    ip_address = ""
    try:
        ip_address = subprocess.check_output("ipconfig getifaddr en0", shell=True).strip()
    except:
        ip_address = "ERROR"
        
    deploymentid_file = '/Users/check/Desktop/maintenance_daemon-master/deploymentid.txt'
    deploymentid = "ERROR"
    try:
        if os.path.isfile(deploymentid_file):
            with open(deploymentid_file, "r") as f:
                local_deploymentid = f.readlines()[0]
            ref = db.reference(local_deploymentid)
            firebase_deploymentid = ref.child("Deployment ID").get()
            if firebase_deploymentid == None:
                print("Updating firebase with local deploymentid")
                deploymentid = local_deploymentid
            elif firebase_deploymentid != local_deploymentid:
                print("ERROR: deployment id mismatch")
                deploymentid = "ERROR - local: " + str(local_deploymentid) + " Firebase: " + str(firebase_deploymentid)
            else:
                print("local deploymentid matched successfully")
                deploymentid = firebase_deploymentid
        else:
            deploymentid = host_name + "=" + "-".join(ip_address.split(".")[2:])
            local_deploymentid = deploymentid
            ref = db.reference(local_deploymentid)
            print("Creating deploymentid file with id: " + deploymentid)
            with open(deploymentid_file, "w") as f:
                f.write(deploymentid)
    except Exception as e:
        if not local_deploymentid:
            local_deploymentid = "ERROR-caught: " + str(e)
        else:
            ref = db.reference(local_deploymentid)
        deploymentid = "ERROR-caught: " + str(e)
    
    if not local_deploymentid or "ERROR" in deploymentid or "ERROR" in local_deploymentid:
        print("local_deploymentid: " + local_deploymentid)
        print("deploymentid: " + deploymentid)
        subprocess.check_output("echo 'Maintenance Script Deployment ID Error' > '/opt/automation-scripts/logs/stderr/maintenance_script.txt'", shell=True)
        if not local_deploymentid:
            print("Local Deployment ID Error. Exiting...")
            exit(1)

    
    dep_date_label = "Deployment Date"
    if ref.child(dep_date_label).get() == None:
        ref.child(dep_date_label).set(date.today().strftime("%b-%d-%Y"))

    # Variables to collect:
    number_concurrent_users = "N/A"
    munki_manifest = ""
    try:
        munki_manifest = subprocess.check_output('defaults read /Library/Preferences/ManagedInstalls ClientIdentifier',
                                                 shell=True).decode("utf-8").strip()
    except:
        munki_manifest = "error"


    qos_trigger = "Yes"

    try:
        backup_location = subprocess.check_output("tmutil destinationinfo | grep Name | cut -d \':\' -f 2",
                                              shell=True).strip()
    except:
        backup_location = "ERROR"
        
    try:
        last_backup = subprocess.check_output(
            '''date -j -f "%b %d %T %Y" "$(/usr/libexec/PlistBuddy -c "Print Destinations:0:SnapshotDates" /Library/Preferences/com.apple.TimeMachine.plist | tail -n 2 | head -n 1 | awk '{ print $2,$3,$4,$6 }')" "+%m-%d-%Y %I:%M %p"''',
            shell=True).strip()
    except:
        last_backup = "0"
    if last_backup == "":
        last_backup = "0"
    free_storage = subprocess.check_output("""df -k / | awk 'NR==2 { printf "%d", int($4/1000000) }'""", shell=True)
    last_reboot_time = subprocess.check_output(
        '''sysctl kern.boottime | awk '{ printf "%s-%s-%s %s", $(NF-3), $(NF-2), $NF, $(NF-1) }' | cut -d ':' -f 1,2''',
        shell=True).strip()
    os_version = subprocess.check_output("sw_vers -productVersion", shell=True).strip()
    model = subprocess.check_output(
        '''curl -s "https://support-sp.apple.com/sp/product?cc=$(system_profiler SPHardwareDataType | awk '/Serial/ {print $4}' | cut -c 9-)" | sed "s|.*<configCode>\(.*\\)</configCode>.*|\\1|"''',
        shell=True).strip()
    total_storage = ""
    try:
        total_storage = subprocess.check_output("""df -k / | awk 'NR==2 { printf "%d", int(int($3+$4)/1000000) }'""", shell=True).strip()
    except:
        total_storage = ""
    
    try:
        if os.path.isfile("/Users/temp/Desktop/unprimed.txt"):
            state = "unprimed"
        else:
            old_state = ref.child("State").get()
            if old_state is None or old_state == "" or "unprimed" in old_state:
                state = "active"
                print("No existing state or unprimed state. Setting state to active.")
            elif old_state != "active":
                if "error" in old_state:
                    state = old_state
                else:
                    state = "error: " + old_state
                print("State mismatch detected: " + state)
            else:
                state = old_state
                print("active state verified successfully.")
    except Exception as e:
        print("Error checking state: " + str(e))

    update_count = 0
    try:
        update_count = subprocess.check_output("softwareupdate -l | grep '*' | wc -l", shell=True).strip()
    except:
        update_count = "error"
    try:
        update_xcode = subprocess.check_output("sudo xcodebuild -runFirstLaunch", shell=True)
    except:
        pass
    try:
        update_xcode = subprocess.check_output(
            "sudo xcode-select -s /Applications/Xcode.app/Contents/Developer; sudo xcodebuild -runFirstLaunch",
            shell=True)
    except:
        pass

    reboot_list = subprocess.check_output("last reboot | grep -v wtmp | awk '{print $4,$5}'", shell=True).splitlines()
    reboot_count = 0
    seven_days_ago = (datetime.today() - timedelta(days=7))
    for r in reboot_list:
        if r == ' ' or not r or r == '':
            continue
        try:
            r_date = datetime.strptime(r, '%b %d')
        except:
            print("Conversion error. r value: " + r)
            break
        current_month = datetime.today().month
        if r_date.month not in (current_month, current_month - 1):
            break

        # if it is January or February and the 'last reboot' month is December (since last reboot doesn't print year)
        if current_month in (1, 2) and r_date.month == 12:
            r_date = r_date.replace(year=datetime.today().year - 1)  # set year to last year
        else:
            r_date = r_date.replace(year=datetime.today().year)
        if r_date > seven_days_ago:
            reboot_count += 1

    drive_test_label = "Drive Write MB per sec"
    current_drive_test_value = ref.child(drive_test_label).get()
    if current_drive_test_value == None or current_drive_test_value == "error":
        if (StrictVersion(os_version) < StrictVersion("10.13.0")):
            drive_write = "macOS 10.13.0+ required"
            drive_read = "macOS 10.13.0+ required"
        else:
            print "Testing storage drive..."
            drive_test = subprocess.check_output(
                "unzip -n -q '/Users/check/Desktop/maintenance_daemon-master/Blackmagic Disk Speed Test.app.zip' -d '/Users/check/Desktop'; sudo chmod +x '/Users/check/Desktop/Blackmagic Disk Speed Test.app/Contents/MacOS/DiskSpeedTest'; cd '/Users/check/Desktop/maintenance_daemon-master'; sudo chmod +x './drive-test'; './drive-test'; sudo rm -rf '/Users/check/Desktop/Blackmagic Disk Speed Test.app'",
                shell=True).splitlines()
            try:
                drive_write = str(drive_test[0].split(",")[1])
                drive_read = str(drive_test[1].split(",")[1])
            except:
                drive_write = "error"
                drive_read = "error"

        ref.child(drive_test_label).set(drive_write)
        ref.child("Drive Read MB per sec").set(drive_read)
    
    global tmectl_info
    try:
        my_timer = Timer(30, check_backup)
        my_timer.start()
        my_timer.join()

        if not tmectl_info:
            raise ValueError('tmectl_info could not read backup data')
    except Exception as e:
        print('Reconfiguring time machine editor. error: ' + str(e))
        try:
            command = (
                '''echo "Installing TimeMachineEditor";'''
                '''softwareupdate --install-rosetta --agree-to-license 2> /dev/null || :;'''
                '''ip=$(/sbin/ifconfig | grep "inet " | grep -v 127.0.0.1 | cut -d\\  -f2 | cut -d . -f 4 | awk '{ print $1 }' | tail -n 1);'''
                '''hour_res=$(expr $ip % 2 + 3);'''
                '''min_res=$(expr $ip % 40 + 10);'''
                '''_first="2";'''
                '''_second="4";'''
                '''_third="6";'''
                '''if $(( $ip % 2 == 0 )); then'''
                '''    _first="1";'''
                '''    _second="3";'''
                '''    _third="5";'''
                '''fi;'''
                '''echo "Hour to be set: ${hour_res}";'''
                '''echo "Minute to be set: ${min_res}";'''
                '''echo "First to be set: ${_first}";'''
                '''echo "Second to be set: ${_second}";'''
                '''echo "First to be set: ${_third}";'''
                '''export hour_res;'''
                '''export min_res;'''
                '''export _first;'''
                '''export _second;'''
                '''export _third;'''
                '''xml="/users/check/Desktop/maintenance_daemon-master/tmectl.xml";'''
                '''/usr/bin/perl -pi -e 's/_hour/$ENV{hour_res}/' "$xml";'''
                '''/usr/bin/perl -pi -e 's/_min/$ENV{min_res}/' "$xml";'''
                '''/usr/bin/perl -pi -e 's/_first/$ENV{_first}/' "$xml";'''
                '''/usr/bin/perl -pi -e 's/_second/$ENV{_second}/' "$xml";'''
                '''/usr/bin/perl -pi -e 's/_third/$ENV{_third}/' "$xml";'''
                '''sudo mount -uw /;'''
                '''sudo /usr/local/bin/tmectl uninstall;'''
                '''tmpdir="${TMPDIR:-/tmp}";'''
                '''curl -o "${tmpdir}/tmeclt.pkg" https://tclementdev.com/timemachineeditor/TimeMachineEditor508.pkg;'''
                '''/usr/sbin/installer -pkg "${tmpdir}/tmeclt.pkg" -target /;'''
                '''sudo /usr/bin/tmutil disable;'''
                '''sudo /usr/local/bin/tmectl updates check -i;'''
                '''echo "Waiting 10 Seconds for service to start...";'''
                '''sleep 5;'''
                '''echo "5 More Seconds...";'''
                '''sleep 5;'''
                '''echo Done...;'''
                '''sudo /usr/local/bin/tmectl settings import "$xml";'''
                '''sudo /usr/local/bin/tmectl info;'''
            )
            subprocess.check_output(command, executable="/bin/bash", stderr=subprocess.STDOUT, shell=True)
            my_timer.start()
            my_timer.join()

            if not tmectl_info:
                raise ValueError('tmectl_info could not read backup data')
        except:
            tmectl_info = "Installation Error"

    sip_status = ""
    try:
        sip_status = subprocess.check_output("csrutil status 2>&1", shell=True).encode("utf-8").strip()
    except:
        sip_status = "error"

    try:
        mdm_status = subprocess.check_output("sudo /usr/bin/profiles -P", shell=True)
        if "mosyle" in mdm_status:
            mdm_status = "Enrolled"
        else:
            mdm_status = "Not Enrolled"
    except:
        mdm_status = "Not Enrolled"

    # check NuoRDS permissions
    rds_perm_check = ""
    try:
        if (os_version.split(".", 2)[:2] == ['10', '15']):
            print("Catalina detected - checking NuoRDS permissions")
            rds_acc_perm = subprocess.check_output(
                """sqlite3 /Library/Application\ Support/com.apple.TCC/TCC.db 'select client from access where allowed and service = "kTCCServiceAccessibility"' | grep com.nuords.nrdagent""",
                shell=True).encode("utf-8").strip()
            rds_screen_perm = subprocess.check_output(
                """sqlite3 /Library/Application\ Support/com.apple.TCC/TCC.db 'select client from access where allowed and service = "kTCCServiceScreenCapture"' | grep com.nuords.nrdagent | tail -1""",
                shell=True).encode("utf-8").strip()
        if (os_version[:2] == "11"):
            print("Big Sur detected - checking NuoRDS permissions")
            rds_acc_perm = subprocess.check_output(
                """sqlite3 /Library/Application\ Support/com.apple.TCC/TCC.db 'select client from access where service = "kTCCServiceAccessibility"' | grep com.nuords.nrdagent""",
                shell=True).encode("utf-8").strip()
            rds_screen_perm = subprocess.check_output(
                """sqlite3 /Library/Application\ Support/com.apple.TCC/TCC.db 'select client from access where service = "kTCCServiceScreenCapture"' | grep com.nuords.nrdagent | tail -1""",
                shell=True).encode("utf-8").strip()

            # set rds_perm_check value
        if "com.nuords.nrdagent" in rds_acc_perm:
            rds_perm_check = "A_OK"
        else:
            rds_perm_check = "A_ERROR"
        if "com.nuords.nrdagent" in rds_screen_perm:
            rds_perm_check = rds_perm_check + " S_OK"
        else:
            rds_perm_check = rds_perm_check + " S_ERROR"
    except:
        rds_acc_perm = "error"

    munki_output = ""
    try:
        munki_output = subprocess.check_output('sudo -u temp /usr/local/munki/managedsoftwareupdate --checkonly',
                                               stderr=subprocess.STDOUT, shell=True).strip()
    except Exception, e:
        munki_output = str(e.output).strip()
        try:
            if "You must run this as root" not in munki_output:
                print("Reinstalling munki")
                munkiScript = """tmpdir="${TMPDIR:-/tmp}"; cd $tmpdir && curl -sLOJ $(curl -s https://api.github.com/repos/munki/munki/releases/latest | grep browser_download_url | cut -d '"' -f 4); sudo installer -pkg ./munkitools-*.pkg -target /"""
                subprocess.check_output(munkiScript, executable="/bin/bash", shell=True)

                try:
                    munki_output = subprocess.check_output(
                        'sudo -u temp /usr/local/munki/managedsoftwareupdate --checkonly', stderr=subprocess.STDOUT,
                        shell=True).strip()
                except Exception, e:
                    munki_output = str(e.output).strip()
                    if "You must run this as root" not in munki_output:
                        munki_output = "ERROR"
                        print("Munki reinstallation failed: " + munki_output)
        except:
            munki_output = "ERROR"
            print("Munki reinstallation failed")

    
    global user_storage
    user_storage = []
    try:
        if int(free_storage) < 300:
            print("Calculating top 5 largest user folders...")
            user_storage = subprocess.check_output(r"""du -ks /Users/* | awk '{ printf("%.2f:%s\n", ($1 / 1000000),$2) }' | sort -n -r | head -n 5 | sed -e 's`/Users/``g' -e $'s/ *\t/  /g'""", shell=True).encode("utf-8").splitlines() 
            #def get_user_storage():
            #    global user_storage
            #    user_storage = subprocess.check_output(r"""du -ks /Users/* | awk '{ printf("%.2f:%s\n", ($1 / 1000000),$2) }' | sort -n -r | head -n 5 | sed -e 's`/Users/``g' -e $'s/ *\t/  /g'""", shell=True).encode("utf-8").splitlines() 

            #thread = Thread(target=get_user_storage) 
            #thread.start()
            #thread.join(300) # time out after 5 minutes

            if not user_storage:
                raise ValueError('Failed to retrieve user storage')
        else:
            print("Free storage over 300GB, skipping user folder check")
    except:
        pass
    user_storage = user_storage[:5] + ["0:0"] * (5 - len(user_storage))  # ensure list length is 5 to avoid index out of range

    firmware_version = ""
    bridge_version = ""
    try:
        firmware_version = subprocess.check_output(
            "system_profiler SPHardwareDataType | grep Version | awk '{print $4}'", shell=True).strip().replace("\n", " ")
        bridge_version = subprocess.check_output(
            "system_profiler SPHardwareDataType | grep Version | awk '{print $6}' | tr -d ')'", shell=True).strip()
    except:
        firmware_version = "error"
        bridge_version = "error"

    try:
        subprocess.check_output("sudo rm -rf /Library/KindlePreviewerUpdater/", shell=True)
        print("Setting ARD permissions")
        def set_ard():
            FNULL = open(os.devnull, 'w')
            subprocess.Popen("for user in $(dscl . -list /Users dsAttrTypeNative:naprivs | awk '{print $1}'); do sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -configure -privs -DeleteFiles -ControlObserve -TextMessages -ShowObserve -OpenQuitApps -RestartShutDown -SendFiles -ChangeSettings -users \"$user\"; done; sudo rm -f \"/Library/Preferences/com.apple.ARDAgent.plist\";sudo chmod 000 /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Support/build_hd_index", shell=True, stdout=FNULL, stderr=FNULL)
        thread = Thread(target=set_ard)
        thread.setDaemon = True
        thread.start()
        thread.join(300)
    except Exception as e:
        print("Error: " + str(e))

    cocoapods_output = ""
    try:
        cocoapods_output = subprocess.check_output(
            """if [ -f "/usr/local/bin/rbenv" ]; then export PATH="/usr/local/opt/ruby/bin:$PATH"; fi; if [ -d "/opt/homebrew/opt/ruby/bin" ]; then export PATH="/opt/homebrew/opt/ruby/bin:$PATH"; fi; gem list | grep "cocoapods (" | head -1 2> /dev/null""",
            shell=True).strip()
    except:
        cocoapods_output = "ERROR"

    try:
        pod_init_check = subprocess.check_output("export LANG=en_US.UTF-8; /usr/local/bin/pod init --allow-root 2>&1; exit 0", stderr=subprocess.PIPE, shell=True).strip()
        if pod_init_check == "[!] No Xcode project found, please specify one":
            pod_init_check = "OK"
        else:
            pod_init_check = "ERROR"
    except Exception as e:
        print("pod init error: " + str(e))
        pod_init_check = "EXCEPTION"

    cocoapods_output = pod_init_check + " - " + cocoapods_output

    serial = ""
    try:
        serial = subprocess.check_output("system_profiler SPHardwareDataType | awk '/Serial/ {print $4}'",
                                         shell=True).strip()
    except:
        pass


    afterreboot_version = ""
    try:
        afterreboot_version = subprocess.check_output("""script='/opt/automation-scripts/AfterReboot/afterreboot'; 
        if [ -f "$script" ]; then "$script" -v | awk '{print $3}'; fi""", shell=True).strip()
        if not afterreboot_version:
            afterreboot_version = "Not Installed"
    except:
        afterreboot_version = "error"

    selfheal_version = ""
    try:
        selfheal_version = subprocess.check_output("""script='/opt/automation-scripts/SelfHeal/selfheal'; 
        if [ -f "$script" ]; then "$script" -v | awk '{print $3}'; fi""", shell=True).strip()
        if not selfheal_version:
            selfheal_version = "Not Installed"
    except:
        selfheal_version = "error"

    errormonitor_version = ""
    try:
        errormonitor_version = subprocess.check_output("""script='/opt/automation-scripts/ErrorMonitor/errormonitor'; 
        if [ -f "$script" ]; then "$script" -v | awk '{print $3}'; fi""", shell=True).strip()
        if not errormonitor_version:
            errormonitor_version = "Not Installed"
    except:
        errormonitor_version = "error"

    timezone = "error"
    try:
        timezone = subprocess.check_output("sudo systemsetup -gettimezone", shell=True).strip()
    except:
        pass

    xcode_modules_permissions = "none"
    try:
        if os.path.isdir('/Applications/Xcode.app/Contents/SharedFrameworks/ContentDeliveryServices.framework/Versions/A/itms/modules/'):
            xcode_modules_permissions = subprocess.check_output("stat -f '%OLp' '/Applications/Xcode.app/Contents/SharedFrameworks/ContentDeliveryServices.framework/Versions/A/itms/modules/'* | uniq", shell=True).strip()
            if "600" in xcode_modules_permissions:
                xcode_modules_permissions = "itms/modules/* .jar error permissions 600"
            elif xcode_modules_permissions == "644":
                xcode_modules_permissions = "OK (644 .jar permissions)"
        else:
            xcode_modules_permissions = "no itms/modules folder"
    except Exception as e:
        xcode_modules_permissions = "error: " + str(e)

    users_permissions = "none"
    try:
        users_permissions = subprocess.check_output("ls -la / | grep Users | awk -v z='owner:' -v g='group:' '{print $1, z$3, g$4}'", shell=True).strip()
    except Exception as e:
        users_permissions = "error: " + str(e)
    
    unknown_users = "none"
    try:
        unknown_users = subprocess.check_output("dscl . list /Users | grep -vE 'user|_|check|temp|nobody|root|daemon|admin' | paste -s -d, -", shell=True).strip()
    except Exception as e:
        unknown_users = "error: " + str(e)

    nuords_version = "none"
    try:
        # can use the 'witch' command to find exact binary location since nrdservice may not be in the 'root' user's path and this script runs as root
        # i.e. witch nrdservice
        nuords_version = subprocess.check_output("/usr/local/bin/nrdservice version | awk -F'[()]' '{print $2}'", shell=True).strip()
    except Exception as e:
        nuords_version = "error: " + str(e)

    nuords_license = "none"
    try:
        nuords_license = subprocess.check_output("""os_version=$(sw_vers -productVersion); minor_os_version=${os_version#*.}; if [ "${os_version%%.*}" -gt 10 ] || [ "${minor_os_version%.*}" -gt 14 ]; then   license_command="license"; else   license_command="liccmd"; fi; /usr/local/bin/nrdservice "$license_command" check -i | sed -n -e 's/^.*Support Expires: //p'""", shell=True).strip()
    except Exception as e:
        nuords_license = "error: " + str(e)

    activation_lock_status = "none"
    try:
        activation_lock_status =  subprocess.check_output("system_profiler SPHardwareDataType -detailLevel basic | awk '/Activation Lock Status/ {print $4}'", shell=True).strip()
    except Exception as e:
        activation_lock_status = "error: " + str(e)

    selfheal_restarts = "none"
    try:
        ssh_restarts = subprocess.check_output("cat /opt/automation-scripts/SelfHeal/alert-count-logs-firebase/SSH 2>/dev/null || echo '0'", shell=True).strip()
        rdp_restarts = subprocess.check_output("cat /opt/automation-scripts/SelfHeal/alert-count-logs-firebase/RDP 2>/dev/null || echo '0'", shell=True).strip()
        selfheal_restarts = "SSH: " + ssh_restarts + " | " + "RDP: " + rdp_restarts
    except Exception as e:
        selfheal_restarts = "error: " + str(e)

    server_data = {
        "Backup Location": backup_location,
        "Date of Last Backup": last_backup,
        "Free Space (in GB)": free_storage,
        "Name": host_name,
        "IP Address": ip_address,
        "Last Reboot Time": last_reboot_time,
        "Munki Manifest": str(munki_manifest),
        "Number of Concurrent Users": str(number_concurrent_users),
        "OS Version": os_version,
        "QOS Reboot reason": str(reboot_reason),
        "QOS Script Version": version_number,
        "QOS triggered Monitor": str(qos_trigger),
        "Time Ran": time.strftime('%b-%d-%Y %I:%M%p %Z', time.localtime(time.time())),
        "Total Storage": total_storage,
        "Available Updates": update_count,
        "Avg Reboots Daily": reboot_count / 7,
        "Model": model,
        "State": state,
        "User 1": user_storage[0].split(":")[1],
        "User 1 Storage in GB": user_storage[0].split(":")[0],
        "User 2": user_storage[1].split(":")[1],
        "User 2 Storage in GB": user_storage[1].split(":")[0],
        "User 3": user_storage[2].split(":")[1],
        "User 3 Storage in GB": user_storage[2].split(":")[0],
        "User 4": user_storage[3].split(":")[1],
        "User 4 Storage in GB": user_storage[3].split(":")[0],
        "User 5": user_storage[4].split(":")[1],
        "User 5 Storage in GB": user_storage[4].split(":")[0],
        "tmectl info": tmectl_info,
        "NuoRDS Permissions": rds_perm_check,
        "SIP Status": sip_status,
        "MDM Status": mdm_status,
        "Munki Status": munki_output,
        "Firmware": firmware_version,
        "BridgeOS": bridge_version,
        "Cocoapods": cocoapods_output,
        "Serial": serial,
        "AfterReboot": afterreboot_version,
        "SelfHeal": selfheal_version,
        "ErrorMonitor": errormonitor_version,
        "Deployment ID": deploymentid,
        "Time Zone": timezone,
        "Xcode Modules": xcode_modules_permissions,
        "Users Folder": users_permissions,
        "Unknown Users": unknown_users,
        "NuoRDS Version": nuords_version,
        "NuoRDS Expires": nuords_license,
        "Activation Lock": activation_lock_status,
        "SelfHeal Restarts": selfheal_restarts
    }

    if ref.get() is None:
        ref.set(server_data)
    else:
        ref.update(server_data)

    print(server_data)


def main():
    if sys.argv[1] == '-c':
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Maintenance Check Requested by Daemon" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        report_to_monitor("Maintenance Check Script ran - Automated Run")
        check_rsync()
        try:
            set_reboot()
        except Exception as e:
            print("Error setting reboot time: " + str(e))
        check_reboot_script()
    elif sys.argv[1] == '-ec':
        subprocess.Popen(
            ['date +"%m %d %Y %I:%M %p: Error Check Requested" >> /users/check/desktop/maintenance_log.txt'],
            shell=True, stdout=subprocess.PIPE)
        # syntax_verify()
    elif sys.argv[1] == '-version':
        print(version_number)
    elif sys.argv[1] == '-random':
        reset_reboot_random()
    elif sys.argv[1] == '-v':
        print(version_number)
    elif sys.argv[1] == '-monitor':
        report_to_monitor("Run Through Maintenance Script - Manually Activated")
        sys.stdout.close()
        sys.stderr.close()
        sys.exit()
    elif sys.argv[1] == '-m':
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Maintenance Check Requested Manually" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        check_rsync()
        check_reboot_script()
    elif sys.argv[1] == '-r':
        subprocess.Popen(
            ['date +"%m %d %Y %I:%M %p: Force Reboot Requested" >> /users/check/desktop/maintenance_log.txt'],
            shell=True, stdout=subprocess.PIPE)
        force_reboot()
    elif sys.argv[1] == '-reboot':
        subprocess.Popen(
            ['date +"%m %d %Y %I:%M %p: Standard Reboot Requested" >> /users/check/desktop/maintenance_log.txt'],
            shell=True, stdout=subprocess.PIPE)
        graceful_reboot_attempt()
    elif sys.argv[1] == '-u':
        subprocess.Popen(
            ['date +"%m %d %Y %I:%M %p: Returning Update Version" >> /users/check/desktop/maintenance_log.txt'],
            shell=True, stdout=subprocess.PIPE)
        # check_for_updates()
    elif sys.argv[1] == '-e':
        subprocess.Popen([
                             'date +"%m %d %Y %I:%M %p: Force new git and daemon Requested" >> /users/check/desktop/maintenance_log.txt'],
                         shell=True, stdout=subprocess.PIPE)
        reset_daemon_and_git()
    else:
        print("The flag you have run is not an available option")
        print("Usage [-c]: Check (as Daemon) if server has been rebooted within 24 hours")
        print("Usage [-m]: Check (as User) if server has been rebooted within 24 hours")
        print("Usage [-r]: Force the reboot of the server")
        print("Usage [-reboot]: Force the reboot of the server")
        print("Usage [-v] or [-version]: Return Version Number of Script")
        # print("Usage [-e]: Force new git and daemon")
        sys.exit()


if __name__ == "__main__":
    main()
    try:
        sys.stdout.close()
    except:
        pass
    try:
        sys.stderr.close()
    except:
        pass
    sys.exit()
