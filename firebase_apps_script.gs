// Author: Austin Agronick

// Libraries: 
// https://sites.google.com/site/scriptsexamples/new-connectors-to-google-services/firebase/reference
// https://github.com/googleworkspace/apps-script-oauth2


// create button to run script in sheet
function onOpen() {
  SpreadsheetApp.getUi().createMenu('Firebase')
  .addItem('Update Firebase', 'updateFirebaseSheet')
  //.addSeparator()
  //.addItem('Update LA', 'updateLASheet')
  .addToUi();
}

// configure firebase service authentication
function getFirebaseService() {
  return OAuth2.createService('Firebase')
      // Set the endpoint URL.
      .setTokenUrl('https://accounts.google.com/o/oauth2/token')

      // Set the private key and issuer.
      .setPrivateKey([INSERT PRIVATE KEY HERE]) // deprecated. see for alternative: https://stackoverflow.com/questions/38264233/alternative-to-firebase-secret-in-firebase-3-x
      .setIssuer([INSERT ISSUER SERVICE ACCOUNT HERE])

      // Set the property store where authorized tokens should be persisted.
      .setPropertyStore(PropertiesService.getScriptProperties())

      // Set the scopes.
      .setScope('https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/firebase.database');
}

// returns firebase instance
function getFirebase() {
  // generate Google OAuth2 access token from service account
  var token = getFirebaseService().getAccessToken(); 
  const url = "https://serene-flare-296520.firebaseio.com/";
  return FirebaseApp.getDatabaseByUrl(url, token);
}

// update the spreadsheet
function updateFromFirebase(sheetName, region) {
  // get firebase instance and collection from firestore
  const firebase = getFirebase();
  const data = firebase.getData();
  
  // column headers
  var object = Object.entries(data);
  var headers = [['Deployment ID', 'Avg Reboots Daily', 'Available Updates', 'Date of Last Backup', 'Inactive Cleanup Date', 'Inactive Cleanup Result', 'Free Storage Pre-Inactive-Cleanup', 'Free Space (in GB)', 'Total Storage', 'Backup Location', 'Last Reboot Time', 'IP Address', 'OS Version', 'Model', 'QOS Script Version', 'Time Ran', 'State', 'Deployment Date', 'Free Storage Pre-Trash-Cleanup', 'Trash Cleanup Date', 'Trash Cleanup Result', 'Drive Read MB per sec', 'Drive Write MB per sec', 'User 1', 'User 1 Storage in GB', 'User 2', 'User 2 Storage in GB', 'User 3', 'User 3 Storage in GB', 'User 4', 'User 4 Storage in GB', 'User 5', 'User 5 Storage in GB', 'tmectl info', 'NuoRDS Permissions', 'SIP Status', 'MDM Status', 'Munki Status', 'Firmware', 'BridgeOS', 'Cocoapods', 'Serial', 'Munki Manifest', 'AfterReboot', 'SelfHeal', 'ErrorMonitor', 'Name', 'Time Zone', 'Xcode Modules', 'Users Folder', 'Unknown Users', 'NuoRDS Version', 'NuoRDS Expires', 'Activation Lock', 'SelfHeal Restarts']];
  
   // get and clear google sheet
  var sheet = SpreadsheetApp.getActive().getSheetByName(sheetName);
  // getRange(start row, start column, number of rows, number of columns)
  if (sheet.getLastRow() > 0) {
    sheet.getRange(1, 1, sheet.getLastRow(), headers[0].length).clearContent();
  }
  
  // write data to sheet
  sheet.getRange(1, 1, 1, headers[0].length).setValues(headers);
  var current_row = 2;
  for (const [server_key, serverData_value] of Object.entries(data)) {
    if (!(region) || server_key.indexOf(region) == 0) {
      server_data = Object.entries(serverData_value)
      if (! serverData_value['Date of Last Backup']) {
        serverData_value['Date of Last Backup'] = '`';
      }

      serverData_value['Deployment ID'] = server_key
      if (serverData_value['Deployment ID'].includes("=")) {
        serverData_value['Deployment ID'] = serverData_value['Deployment ID'].replace("=",'#').replace("-",".");
      }
      for (var i = 0; i < headers[0].length; i++){
        sheet.getRange(current_row, i+1, 1, 1).setValue(serverData_value[headers[0][i]]);
      }
      // write server name (keys)
      //sheet.getRange(current_row, 1, 1, 1).setValue(server_key);
      current_row++;
    }
  }
}

function updateFirebaseSheet() {
  updateFromFirebase('Firebase');
}

function updateLASheet() {
  updateFromFirebase('LA', 'LA');
}



function onSelectionChange(e) {
  var sheetName = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet().getName();
  if (sheetName == "Firebase" || sheetName == "Low Storage") {
    const range = e.range;
    const sheet = range.getSheet();
    const maxRows = sheet.getMaxRows();
    const maxColumns = sheet.getMaxColumns();
  
  sheet.clearConditionalFormatRules();  
  var formula = '=A1<>"1239"'; 
    
  //---------------------------------------------------------------  
  //RULE1  
  var range1 = sheet.getRange(range.getRow(), 1, 1, maxColumns);
  var rule1 = SpreadsheetApp.newConditionalFormatRule()
      .whenFormulaSatisfied(formula)
      .setBackground('lightblue')
      .setBold(true)
      .setRanges([range1])
      .build();
  var rules1 = sheet.getConditionalFormatRules();
  rules1.push(rule1);
  sheet.setConditionalFormatRules(rules1);
  /*--------------------------------------------------------------- 
  //RULE2
  var range2 = sheet.getRange(1, range.getColumn(), maxRows, 1);
  var rule2 = SpreadsheetApp.newConditionalFormatRule()
      .whenFormulaSatisfied(formula)
      .setBackground('lightgray')
      //.setBold(true)
      .setRanges([range2])
      .build();
  var rules2 = sheet.getConditionalFormatRules();
  rules2.push(rule2);
  sheet.setConditionalFormatRules(rules2);  
    */
  }
}
