# firebase_daemon
This script performs the nightly reboot of managed servers and runs various commands to automate server administration. Most importantly this script sends data about managed servers to the [firebase database](https://console.firebase.google.com/project/serene-flare-296520/database/serene-flare-296520/data).

Since this script uses Python 2 and is difficult to control deployment compared to the Munki system, it is reccomended to continue all quality of service related automation using the [ManagedAutomationScripts](https://gitlab.com/briviant/packages/munkipackages/managedautomationscripts) project.  
<br />

The firebase_daemon requires the firebase-admin python package created by Google and available to download [here](https://pypi.org/project/firebase-admin/) (the firebase_admin_module folder in this project is a copy of this download). For reference, the module could also be installed using the command `pip install firebase-admin`. The current known problem affecting this project is the static downloaded folder may be made obsolete by Google. For future improvement the package could be pip installed using Ansible or another mechanism.

It is required that the Firebase Google sheet has the Apps Script enabled. To enable the apps script feature, go to a spreadsheet and click Extensions > Apps Script and develop or 
update the script as needed. See firebase_apps_script.gs for an example of a Google Apps Script used for the Firebase sheet automation. The script uses a time based trigger and the 
setting can be adjust in the Apps Script page by clicking the Triggers button on the left and creating a new time based trigger.

### Updating firebase with additional fields
To create an additional column in the firebase spreadsheet, update the app script by clicking Extensions > Apps Script while at the spreadsheet. Edit the headers variable to include the additional columns. In addition, update the server_data variable in this project's maintenance_script.py with the new firebase column.
After the next reboot, each mac will automatically pull the latest changes from the master branch. Currently the testing and deployment is done through manually connecting into various servers
and running the python code manually. In addition the SSH Toolkit script qosupdate can be used to immidiately push updates to the firebase_daemon master branch.

### FirebaseTools
The [FirebaseTools](https://gitlab.com/briviant/managedservertools/firebasetools) project is used to maintain the firebase data.

### Known Issues
- if the file permissions of the /Users/check/Desktop/maintenance_daemon-master folder and it's contents are not owned by the root user the script can fail
- if any command fails in the script outside of a try-except block, the script can exit before reporting to the firebase
- if a server is offline, the firebase data will not be updated and the server may appear to be not rebooting, not backing up, etc. but it is actually offline
- the daemons become disabled for an unknown reason. the SelfHeal script should mitigate this issue and it can also be manually fixed by running the qosinstall script in the SSH Toolkit
- the daemons require python to have the full disk access permission in System Preferences > Security & Priviacy on all Big Sur and later macOS version servers through the MDM. Enabling this permission on some Catalina servers has been shown to fix daemon failure on some Catalina servers

While the script's features have been greatly expanded on over time, its core design and structure has not changed since the original documentation was created, which can be found here: https://drive.google.com/drive/folders/1_4zI2iQ_qaYD-bxUduI6S1b-C523ocgo?usp=sharing
