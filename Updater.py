#!/usr/bin/python

import sys
import subprocess
from threading import Timer
import argparse
import os
import time
import os.path
from os import path
disabled = '0'

branch = "origin master"
def check_for_updates(branch):
    # if checkvar == 1: print "Checking: check_for_updates" return 0 if path.exists(
    # "/Users/check/Desktop/maintenance_script.py.old") == False: subprocess.Popen(['date +"%m %d %Y %I:%M %p:
    # Checking for Updates - Making backup of current Script" >> /users/check/desktop/maintenance_log.txt'],
    # shell=True, stdout=subprocess.PIPE) subprocess.Popen(['sudo cp
    # /Users/check/Desktop/maintenance_daemon-master/maintenance_script.py
    # /Users/check/Desktop/maintenance_script.py.old'], shell=True, stdout=subprocess.PIPE) else: subprocess.Popen([
    # 'date +"%m %d %Y %I:%M %p: Checking for Updates - Backup of current Script already exists - proceeding with
    # update check" >> /users/check/desktop/maintenance_log.txt'], shell=True, stdout=subprocess.PIPE)
    install_path = "/Users/check/Desktop/maintenance_daemon-master/"
    gitoutputcheck = subprocess.check_output("sudo git -C " + install_path + " fetch origin master; sudo git -C " + install_path + " reset --hard origin/master; sudo git -C " + install_path + " pull --force --ff-only " + branch, shell=True).decode("utf-8")

    file = open("/users/check/desktop/maintenance_log.txt", "a")
    # file.write("Version Number: " + version_number + "\n")
    file.write("Checking for updates: " + gitoutputcheck)
    file.close()
    if "Already up to date" not in str(gitoutputcheck):
        subprocess.Popen(['date +"%m %d %Y %I:%M %p: Update Complete - Next run will run updated Script..." >> /users/check/desktop/maintenance_log.txt'],shell=True, stdout=subprocess.PIPE)
        # subprocess.Popen(['sudo python /Users/check/Desktop/maintenance_daemon-master/maintenance_script.py -c'],shell=True, stdout=subprocess.PIPE)
        time.sleep(5)
    # sys.exit()

def main():
    if len(sys.argv) == 1:
        branch = 'origin master'
    else:
        branch = sys.argv[1]
    check_for_updates(branch)
    try:
        sys.stdout.close()
    except:
        pass
    try:
        sys.stderr.close()
    except:
        pass
    sys.exit()

if __name__ == "__main__":
    main()
